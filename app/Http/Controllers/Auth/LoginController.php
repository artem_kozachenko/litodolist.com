<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\User;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function showLoginForm()
    {
        return view('auth.authentication');
    }
    protected function validateLogin(Request $request)
    {
        $messages = [
            'email_login.required' => 'The email field is required.',
            'email_login.email' => 'The email must be a valid email address.',
            'password_login.required' => 'The password field is required.',
            'password_login.min' => 'The password must be at least 8 characters.'
        ];
        $this->validate($request, [
            $this->username() => 'required|email',
            'password_login' => 'required|min:8',
        ], $messages);
    }
    protected function attemptLogin(Request $request)
    {
        return $this->guard()->attempt(
            ['email' => $request->input($this->username()), 'password' => $request->input('password_login')],
            $request->has('remember')
        );
    }
    public function username()
    {
        return 'email_login';
    }
    public function logout(Request $request)
    {
        $this->guard()->logout();
        $request->session()->invalidate();
        return redirect()->route('authentication');
    }
}
