<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $messages = [
            'email_register.required' => 'The email field is required.',
            'email_register.email' => 'The email must be a valid email address.',
            'email_register.unique' => 'This email has already been taken.',
            'password_register.required' => 'The password field is required.',
            'password_register.min' => 'The password must be at least 8 characters.'
        ];
        return Validator::make($data, [
            'email_register' => 'required|email|unique:users,email',
            'password_register' => 'required|min:8',
            'retype_password' => 'required|min:8|same:password_register',
        ], $messages);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'email' => $data['email_register'],
            'password' => bcrypt($data['password_register']),
        ]);
        return $user;
    }

}
