<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\Task;
use Auth;
use Carbon\Carbon;

class MainController extends Controller
{
    public function index()
    {
        $projects = Project::select('id', 'name', 'color')->where('user_id', '=', Auth::id())->get();
        $projects->each(function($item, $key) {
            $id = $item->id;
            $item->setExpiredTasks($item->tasks()->where([
                    ['done', '=', '0'],
                    ['date', '<', Carbon::today()]
                ])->get());
            $item->setDoneTasks($item->tasks()->where('done', '=', '1')->get());
            $item->setAllTasks($item->tasks()->where([
                    ['done', '=', '0'],
                    ['date', '>=', Carbon::today()],
                ])
                ->orWhere(function ($query) use($id) {
                    $query->whereNull('date')->where([
                        ['done', '=', '0'],
                        ['project_id', '=', $id]
                    ]);
                })
                ->get());
            $item->setTodayTasks($item->tasks()->where([
                    ['done', '=', '0'],
                    ['date', '>=', Carbon::today()],
                    ['date', '<', Carbon::tomorrow()],
                ])->get());
            $item->setNext7DaysTasks($item->tasks()->where([
                    ['done', '=', '0'],
                    ['date', '>=', Carbon::today()],
                    ['date', '<', Carbon::today()->addWeek()],
                ])->get());
        });
        return view('todoList', ['projects' => $projects->toArray()]);
    }
}
