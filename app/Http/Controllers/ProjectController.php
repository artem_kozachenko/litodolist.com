<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use Auth;

class ProjectController extends Controller
{
    public function create(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);
        Project::create([
            'user_id' => Auth::id(),
            'name' => $request->input('name'),
            'color' => $request->input('color'),
        ]);
        return redirect()->back();
    }
    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);
        Project::where('id', '=', $request->input('id'))->update([
            'name' => $request->input('name'),
            'color' => $request->input('color'),
        ]);
        return redirect()->back();
    }
    public function delete(Request $request)
    {
        Project::destroy($request->input('id'));
        return redirect()->back();
    }
}
