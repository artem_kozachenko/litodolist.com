<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use Auth;

class TaskController extends Controller
{
    public function create(Request $request)
    {
        $this->validate($request, [
            'text' => 'required',
        ]);
        Task::create([
            'project_id' => $request->input('project_id'),
            'text' => $request->input('text'),
            'date' => $request->input('date'),
            'priority' => $request->input('priority'),
        ]);
        return redirect()->back();
    }
    public function createForProject(Request $request)
    {
        $this->validate($request, [
            'text' => 'required',
            'project_id' => 'required'
        ]);
        Task::create([
            'project_id' => $request->input('project_id'),
            'text' => $request->input('text'),
            'date' => $request->input('date'),
            'priority' => $request->input('priority'),
        ]);
        return redirect()->back();
    }
    public function update(Request $request)
    {
        $this->validate($request, [
            'text' => 'required',
        ]);
        Task::where('id', '=', $request->input('id'))->update([
            'text' => $request->input('text'),
            'priority' => $request->input('priority'),
            'date' => $request->input('date'),
        ]);
        return redirect()->back();
    }
    public function delete(Request $request)
    {
        Task::destroy($request->input('id'));
        return redirect()->back();
    }
    public function done(Request $request)
    {
        Task::where('id', '=', $request->input('id'))->update([
            'done' => '1',
        ]);
        return redirect()->back();
    }
}
