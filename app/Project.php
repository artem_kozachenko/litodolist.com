<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'name',
        'color',
    ];
    public function setDoneTasks($tasks)
    {
        $this->relations['done'] = $tasks;
    }
    public function setAllTasks($tasks)
    {
        $this->relations['tasks'] = $tasks;
    }
    public function setTodayTasks($tasks)
    {
        $this->relations['todayTasks'] = $tasks;
    }
    public function setNext7DaysTasks($tasks)
    {
        $this->relations['next7DaysTasks'] = $tasks;
    }
    public function setExpiredTasks($tasks)
    {
        $this->relations['expiredTasks'] = $tasks;
    }
    public function tasks()
    {
        return $this->hasMany('App\Task')->orderBy('priority');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function delete()
    {
        $this->tasks()->delete();
        return parent::delete();
    }

}
