$("#log_in_form").validate({
    validClass: 'success',
    errorElement: 'p',
    rules: {
        email_login: {
            required: true,
            email: true
        },
        password_login: {
            required: true,
            minlength: 8
        }
    },
    messages: {
        email_login: {
            required: 'The email field is required',
            email: 'The email must be a valid email value'
        },
        password_login: {
            required: 'The password field is required',
            minlength: 'The password must be at least 8 characters long'
        }
    }
});
$("#registration_form").validate({
    validClass: 'success',
    errorElement: 'p',
    rules: {
        email_register: {
            required: true,
            email: true
        },
        password_register: {
            required: true,
            minlength: 8
        },
        retype_password: {
            required: true,
            equalTo: '#password'
        },
    },
    messages: {
        email_register: {
            required: 'The email field is required',
            email: 'The email must be a valid email value'
        },
        password_register: {
            required: 'The password field is required',
            minlength: 'The password must be at least 8 characters long'
        },
        retype_password: {
            required: 'The retype password field is required',
            equalTo: "Passwords don't match."
        },
    }
});
