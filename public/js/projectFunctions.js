// Submit delete project form
$('button[name="delete_project"]').click(function() {
    $('#delete_project').children('input[name="id"]').val($(this).val());
    $('#delete_project').submit();
});
// Validate create project
$('#create_project').validate({
    rules: {
        name: {
            required: true,
        }
    },
    messages: {
        name: {
            required: '',
        }
    }
});
// Validate update project
$('#update_project').validate({
    rules: {
        name: {
            required: true,
        }
    },
    messages: {
        name: {
            required: '',
        }
    }
});
