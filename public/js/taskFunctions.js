// Submit delete taks form
$('button[name="delete_task"]').click(function() {
    $('#delete_task').children('input[name="id"]').val($(this).val());
    $('#delete_task').submit();
});
// Submit delete taks form
$('button[name="done_task"]').click(function() {
    $('#done_task').children('input[name="id"]').val($(this).val());
    $('#done_task').submit();
});
// Validate cteate task form
$('#create_task').validate({
    rules: {
        text: {
            required: true,
        }
    },
    messages: {
        text: {
            required: '',
        }
    }
});
// Validate cteate task for project form
$('#create_task_for_project').validate({
    rules: {
        text: {
            required: true,
        }
    },
    messages: {
        text: {
            required: '',
        }
    }
});
// Validate update task form
$('#update_task').validate({
    rules: {
        text: {
            required: true,
        }
    },
    messages: {
        text: {
            required: '',
        }
    }
});
