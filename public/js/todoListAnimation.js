var a = 0;
// Show archive
$('li.archive').click(function() {
    $('li.active').not(this).removeClass('active').css('background', 'rgb(230,234,243)').children('a').css('color', 'rgb(51,122,183)');
    $('div.active').removeClass('active');
    $(this).addClass('active');
    $('#archive').addClass('active');
});
// Hide archive
$('ul.nav>li').not('#user').click(function() {
    $('li.archive').removeClass('active').css('background', 'rgb(60,141,188)');
});
// Show delete message
$('button[name="message"]').click(function() {
    $('.wrapper').addClass('blur');
    $('#sure').children('div').children('button[name="delete_project"]').val($(this).val());
    $('#sure').show();
});
// Show delete message
$('button[name="close_message"]').click(function() {
    $('.wrapper').removeClass('blur');
    $('#sure').hide();
});
// Show create task for project form
$('.add-t-p').click(function() {
    var form = $('#create_task_for_project');
    $(form).prev('p').show();
    $(this).hide().after(form).next().show();
});
// Hide create task for project form
$('button[name="close_create_task_for_project"]').click(function() {
    $(this).parent('form').hide().prev('p').show();
});
// Show create taks form
$('.add-t').click(function() {
    var form = $('#create_task');
    var projectId = $(this).attr('class');
    var index = projectId.indexOf(' ');
    projectId = projectId.substring(0, index);
    $(form).prev('p').show();
    $(this).hide().after(form).next().show().children('input[name="project_id"]').val(projectId);
});
// Hide create task form
$('button[name="close_create_task"]').click(function() {
    $(this).parent('form').hide().prev('p').show();
});
// Show edit task form
$('button[name="edit_task"]').click(function() {
    var form = $('#update_task');
    // Set current id
    $(form).children('input[name="id"]').val($(this).val());
    // Set current text
    $(form).children('input[name="text"]').val($(this).parent('ul').prevAll('p').text());
    // Set current date
    $(form).children('input[name="date"]').val($(this).parent('ul').prevAll('span').eq(1).text());
    // Set current priority
    var priority = $(this).parent('ul').prevAll('div').eq(1).attr('class');
    var index = priority.indexOf('-');
    priority = priority.substring(index + 1);
    $(form).children('select').val(priority);
    $(this).parent('ul').parent('div').hide().after(form).next().show().after('<br class="after-task"><br class="after-task">');
});
// Hide edit task form
$('button[name="close_update_task"]').click(function() {
    $(this).parent('form').hide().prev('div').show().nextAll('.after-task').remove();
});
// Hide task menu
$('.box-footer.box-comments').mouseleave(function() {
    $(this).children('.menu').hide();
});
// Show create project form
$('#add-p').click(function() {
    $('#add-p').hide();
    $('#create_project').show();
});
// Show update project form
$('button[name="edit_project"]').click(function() {
    var form = $('#update_project');
    // Set current id
    $(form).children('input[name="id"]').val($(this).val());
    $(form).children('button[name="close_update_project"]').val($(this).val());
    // Set current name
    $(form).children('input[name="name"]').val($(this).parent('ul').prevAll('a').text());
    // Set current Color
    var color = $(this).parent('ul').prevAll('div').attr('class');
    var index = color.indexOf(' ');
    color = color.substring(index + 1);
    $(form).children('select').val(color);
    $(this).parent('ul').parent('li').hide().after(form).next().show().after('<br class="after-project"><br class="after-project">');
});
// Hide update project form
$('button[name="close_update_project"]').click(function() {
    $(this).parent('form').hide().prev('li').show().nextAll('.after-project').remove();
});
// Hide create task form
$('button[name="close_create_project"]').click(function() {
    $(this).parent('form').hide().prev('p').show();
});
// Hide menu
$('li.menu').mouseleave(function() {
    $(this).hide();
});
// Show menu
$('span')
.mouseenter(function() {
    $(this).next().show();
})
.click(function() {
    a = 1;
});
$('span[class="badge"]').click(function() {
    if (a == 0) {
        $('li').not(this).not('#user').css('background', 'rgb(230,234,243)').children('a').css('color', 'rgb(51,122,183)');
    }
    a = 0;
});
$('li')
.click(function() {
    if (a == 0) {
        $('li').not(this).not('#user').not('.user-footer').not('li.archive').css('background', 'rgb(230,234,243)').children('a').css('color', 'rgb(51,122,183)');
    }
    a = 0;
})
.mouseenter(function() {
    $(this).not('#user').children('span').show();
    $(this).not('#user').not('.user-footer').css('background', 'rgb(174,177,184)').children('a').css('color', '#444');
})
.mouseleave(function() {
    $(this).not('#user').children('span').hide();
    $(this).not('#user').children('ul').hide();
    if(!$(this).hasClass('active')) {
        $(this).not('#user').css('background', '').children('a').css('color', 'rgb(51,122,183)');
    }
});
