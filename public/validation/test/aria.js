QUnit.module( "aria" );

QUnit.test( "Invalid field adds aria-invalid=true", function( assert ) {
	var ariaInvalidfirst_name = $( "#ariaInvalidfirst_name" ),
		form = $( "#ariaInvalid" );

	form.validate( {
		rules: {
			ariaInvalidfirst_name: "required"
		}
	} );
	ariaInvalidfirst_name.val( "" );
	ariaInvalidfirst_name.valid();
	assert.equal( ariaInvalidfirst_name.attr( "aria-invalid" ), "true" );
} );

QUnit.test( "Valid field adds aria-invalid=false", function( assert ) {
	var ariaInvalidfirst_name = $( "#ariaInvalidfirst_name" ),
		form = $( "#ariaInvalid" );

	form.validate( {
		rules: {
			ariaInvalidfirst_name: "required"
		}
	} );
	ariaInvalidfirst_name.val( "not empty" );
	ariaInvalidfirst_name.valid();
	assert.equal( ariaInvalidfirst_name.attr( "aria-invalid" ), "false" );
	assert.equal( $( "#ariaInvalid [aria-invalid=false]" ).length, 1 );
} );

QUnit.test( "resetForm(): removes all aria-invalid attributes", function( assert ) {
	var ariaInvalidfirst_name = $( "#ariaInvalidfirst_name" ),
		form = $( "#ariaInvalid" ),
		validator = form.validate( {
			rules: {
				ariaInvalidfirst_name: "required"
			}
		} );

	ariaInvalidfirst_name.val( "not empty" );
	ariaInvalidfirst_name.valid();
	validator.resetForm();
	assert.equal( $( "#ariaInvalid [aria-invalid]" ).length, 0, "resetForm() should remove any aria-invalid attributes" );
} );

