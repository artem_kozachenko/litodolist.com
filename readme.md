To launch the app:

1.   Install all packages 

		composer install
	
2.   Create database

3.   Create and configure .env file

    Some settings:
   
		MAIL_DRIVER=smtp
		MAIL_HOST=smtp.gmail.com
		MAIL_PORT=587
		MAIL_USERNAME=litodolist.com@gmail.com
		MAIL_PASSWORD=qweqweqwe
		MAIL_ENCRYPTION=tls
		APP_URL=http://litodolist.com
		
4.   Clear the config cache

	    php artisan config:clear
		
5.   Generate the key

	    php artisan key:generate
		
6.   Create db tables

	    php artisan migrate