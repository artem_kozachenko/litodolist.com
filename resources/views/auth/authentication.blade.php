<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Authentication</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="{{ URL::asset('adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{ URL::asset('adminlte/bower_components/font-awesome/css/font-awesome.min.css') }}">
        <!-- Ionicons -->
        <link rel="stylesheet" href="{{ URL::asset('adminlte/bower_components/Ionicons/css/ionicons.min.css') }}">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{ URL::asset('adminlte/dist/css/AdminLTE.min.css') }}">
        <!-- iCheck -->
        <link rel="stylesheet" href="{{ URL::asset('adminlte/plugins/iCheck/square/blue.css') }}">
        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{ URL::asset('css/authentication.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('css/validation_styles.css') }}">
    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
            <!-- NAVIGATION TABS -->
            <ul class="nav nav-tabs">
                <li class="active col-md-6 authentication-tabs">
                    <a class="tab-left" href="#login" data-toggle="tab">Sign in</a>
                </li>
                <li class="col-md-6 authentication-tabs">
                    <a class="tab-right" href="#registration" data-toggle="tab">Sign up</a>
                </li>
            </ul>
            <div class="tab-content">
                <!-- SIGB IN -->
                <div class="login-box-body tab-pane active in" id="login">
                    <form method="post" name="log_in_form" id="log_in_form" action="{{ route('login') }}">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('email_login') ? ' has-error' : '' }}">
                            <div class="form-group has-feedback">
                                <input name="email_login" type="text" class="form-control" placeholder="Email" value="{{ old('email_login') }}">
                                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                @if ($errors->has('email_login'))
                                    <span class="help-block">
                                        <p>{{ $errors->first('email_login') }}</p>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('password_login') ? ' has-error' : '' }}">
                            <div class="form-group has-feedback">
                                <input name="password_login" type="password" class="form-control" placeholder="Password">
                                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                @if ($errors->has('password_login'))
                                    <span class="help-block">
                                        <p>{{ $errors->first('password_login') }}</p>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-8">
                                <div class="checkbox icheck">
                                    <label>
                                        <input name="remember" type="checkbox"> Remember Me
                                    </label>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                            </div>
                        </div>
                    </form>
                    <div class="social-auth-links text-center">
                        <p>- OR -</p>
                        <a href="{{ route('googleLogin') }}" class="btn btn-block btn-social btn-google btn-flat">
                            <i class="fa fa-google-plus"></i> Sign in using Google+
                        </a>
                    </div>
                    <a href="{{ route('password.request') }}">I forgot my password</a>
                    <br>
                </div>
                <!-- SIGB UP -->
                <div class="register-box-body tab-pane " id="registration">
                    <form name="registration_form" method="post" id="registration_form" action="{{ route('register') }}">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('email_register') ? ' has-error' : '' }}">
                            <div class="form-group has-feedback">
                                <input name="email_register" type="email" class="form-control" placeholder="Email" value="{{ old('email_register') }}" maxlength="100">
                                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                @if ($errors->has('email_register'))
                                    <span class="help-block">
                                        <p>{{ $errors->first('email_register') }}</p>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('password_register') ? ' has-error' : '' }}">
                            <div class="form-group has-feedback">
                                <input id="password" name="password_register" type="password" class="form-control" placeholder="Password">
                                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                @if ($errors->has('password_register'))
                                    <span class="help-block">
                                        <p>{{ $errors->first('password_register') }}</p>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('retype_password') ? ' has-error' : '' }}">
                            <div class="form-group has-feedback">
                                <input name="retype_password" type="password" class="form-control" placeholder="Retype password">
                                <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                                @if ($errors->has('retype_password'))
                                    <span class="help-block">
                                        <p>{{ $errors->first('retype_password') }}</p>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- jQuery 3 -->
        <script src="{{ URL::asset('adminlte/bower_components/jquery/dist/jquery.min.js') }}"></script>
        <!-- Bootstrap 3.3.7 -->
        <script src="{{ URL::asset('adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
        <!-- Validation -->
        <script src="{{ URL::asset('validation/dist/jquery.validate.js') }}"></script>
        <script src="{{ URL::asset('js/authenticationValidation.js') }}"></script>
        <!-- iCheck -->
        <script src="{{ URL::asset('adminlte/plugins/iCheck/icheck.min.js') }}"></script>
        <script>
            $(function () {
                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    increaseArea: '20%' // optional
                });
            });
        </script>
    </body>
</html>
