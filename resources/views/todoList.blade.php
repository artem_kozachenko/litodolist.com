<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>TodoList</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="{{ URL::asset('adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{ URL::asset('adminlte/dist/css/AdminLTE.min.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('adminlte/dist/css/skins/skin-blue.min.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('css/main.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('css/validation_styles.css') }}">
        <!-- Font Awesome -->
        <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    </head>
    <body class="hold-transition skin-blue ">
        <!-- Message when delete not empty project -->
        <div id="sure" hidden>
            <div id="message">
                <p class="text-center warning">This project contains uncomplited tasks. Are you sure that you want to delete it?</p>
                <button class="btn btn-success col-md-6" type="button" name="delete_project">Yes</button>
                <button class="btn btn-danger col-md-6" type="button" name="close_message">No</button>
            </div>
        </div>
        <!-- div .wrapper -->
        <div class="wrapper">
            <!-- Header -->
            <header class="main-header">
                <a class="logo">
                    <span class="logo-lg"><b>LI</b>TodoList</span>
                </a>
                <nav class="navbar navbar-static-top">
                    <li class="archive"><i class="fas fa-archive"></i></li>
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <li id="user" class="dropdown user user-menu">
                                <a class="dropdown-toggle" data-toggle="dropdown">
                                    <span class="hidden-xs">{{ Auth::user()->email }}</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="user-footer">
                                        <form method="post" action="{{ route('logout') }}">
                                            {{ csrf_field() }}
                                            <button class="btn btn-default btn-flat user-pannel-buttons col-md-12" type="submit" name="button">Sign out</button>
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- End header -->
            @php
                $todayTasks = 0;
                $next7DaysTasks = 0;
                $projectsCounter = 0;
                $allProjects = [];
                foreach($projects as $project) {
                    // Count tasks
                    $todayTasks += count($project['today_tasks']);
                    $next7DaysTasks += count($project['next7_days_tasks']);
                    // Projects id/value array
                    $allProjects[$project['id']] = $project['name'];
                    // Count projects
                    $projectsCounter++;
                }
            @endphp
            <!-- All projects -->
            <div class="projects-container">
                <ul class="nav" role="tablist">
                    <li class="active"><a class="text-center" href="#today" role="tab" data-toggle="pill"><b>Today</b><span class="badge">{{ $todayTasks }}</span></a></li>
                    <li><a class="text-center" href="#next_7_days" role="tab" data-toggle="pill"><b>Next 7 days</b><span class="badge">{{ $next7DaysTasks }}</span></a></li>
                    <br>
                    <p class="title project-title text-center">Projects</p>
                    @foreach($projects as $key => $value)
                        <li id="{{ $value['id'] }}">
                            <div class="circle {{ $value['color'] }}"></div>
                            <a class="project-name" href="#{{ $value['id'] }}_tab" role="tab" data-toggle="pill">{{ $value['name'] }}</a>
                            <span class="glyphicon glyphicon-option-vertical "></span>
                            <ul class="menu" hidden>
                                <button value="{{ $value['id'] }}" name="edit_project" class="col-md-12 btn btn-primary" type="button">Edit</button>
                                @if(empty($value['tasks']) && empty($value['expired_tasks']))
                                    <button value="{{ $value['id'] }}" name="delete_project" class="col-md-12 btn btn-danger" type="button">Delete</button>
                                @else
                                    <button value="{{ $value['id'] }}" name="message" class="col-md-12 btn btn-danger" type="button">Delete</button>
                                @endif
                            </ul>
                        </li>
                     @endforeach
                </ul>
                <br>
                <p class="text-center" id="add-p">+Add project</p>
                <!-- Create project form -->
                <form id="create_project" action="{{ route('projectCreate') }}" method="post" hidden>
                    {{ csrf_field() }}
                    <input type="text" class="form-control" name="name" placeholder="Project name">
                    <select name="color">
                        <option value="grey">Grey</option>
                        <option value="yellow">Yellow</option>
                        <option value="green">Green</option>
                        <option value="blue">Blue</option>
                        <option value="black">Black</option>
                        <option value="red">Red</option>
                    </select>
                    <br>
                    <button class="col-md-6 btn btn-primary" type="submit">Create</button>
                    <button name="close_create_project" class="col-md-6 btn btn-danger" type="button">Close</button>
                </form>
                <!-- Update project form -->
                <form id="update_project" action="{{ route('projectUpdate') }}" method="post" hidden>
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="">
                    <input class="form-control" type="text" name="name" placeholder="Enter new name">
                    <select name="color">
                        <option value="grey">Grey</option>
                        <option value="yellow">Yellow</option>
                        <option value="green">Green</option>
                        <option value="blue">Blue</option>
                        <option value="black">Black</option>
                        <option value="red">Red</option>
                    </select>
                    <br>
                    <button name="update" class="col-md-6 btn btn-primary" type="submit">Update</button>
                    <button name="close_update_project" class="col-md-6 btn btn-danger" type="button">Close</button>
                </form>
                <!-- Delete project form -->
                <form id="delete_project" action="{{ route('projectDelete') }}" method="post" hidden>
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="">
                </form>
            </div>
            <!-- End all projects -->
            <!-- All tasks -->
            <div class="tab-content">
                <!-- Today tasks -->
                <div class="tasks-container tab-pane active" role="tabpanel" id="today">
                    @foreach($projects as $project)
                        @foreach($project['today_tasks'] as $task)
                            <div class="box-footer box-comments">
                                <div class="square-{{ $task['priority'] }}"></div>
                                <span class="task-text">{{ $project['name'] }}</span>
                                <p class="task-text">{{ $task['text'] }}</p>
                                <div class="circle {{ $project['color'] }}"></div>
                                <span class="date">{{ $task['date'] }}</span>
                                <span class="glyphicon glyphicon-option-vertical"></span>
                                <ul class="menu">
                                    <button name="edit_task" value="{{ $task['id'] }}" class="col-md-12 btn btn-primary" type="button">Edit</button>
                                    <button name="done_task" value="{{ $task['id'] }}" class="col-md-12 btn btn-success" type="button">Done</button>
                                    <button name="delete_task" value="{{ $task['id'] }}" class="col-md-12 btn btn-danger" type="button">Delete</button>
                                </ul>
                            </div>
                        @endforeach
                    @endforeach
                    @if($projectsCounter != 0)
                        <br>
                        <p class="add-t-p">+Add task</p>
                    @endif
                </div>
                <!-- End today tasks -->
                <!-- Next 7 days tasks -->
                <div class="tasks-container tab-pane" role="tabpanel" id="next_7_days">
                    @foreach($projects as $project)
                        @foreach($project['next7_days_tasks'] as $task)
                            <div class="box-footer box-comments">
                                <div class="square-{{ $task['priority'] }}"></div>
                                <span class="task-text">{{ $project['name'] }}</span>
                                <p class="task-text">{{ $task['text'] }}</p>
                                <div class="circle {{ $project['color'] }}"></div>
                                <span class="date">{{ $task['date'] }}</span>
                                <span class="glyphicon glyphicon-option-vertical"></span>
                                <ul class="menu">
                                    <button name="edit_task" value="{{ $task['id'] }}" class="col-md-12 btn btn-primary" type="button">Edit</button>
                                    <button name="done_task" value="{{ $task['id'] }}" class="col-md-12 btn btn-success" type="button">Done</button>
                                    <button name="delete_task" value="{{ $task['id'] }}" class="col-md-12 btn btn-danger" type="button">Delete</button>
                                </ul>
                            </div>
                        @endforeach
                    @endforeach
                    @if($projectsCounter != 0)
                        <br>
                        <p class="add-t-p">+Add task</p>
                    @endif
                </div>
                <!-- End next 7 days tasks -->
                <!-- Archive -->
                <div class="tasks-container tab-pane" role="tabpanel" id="archive">
                    @foreach($projects as $project)
                            @foreach($project['done'] as $task)
                            <div class="box-footer box-comments">
                                <div class="square-{{ $task['priority'] }}"></div>
                                <span class="task-text">{{ $project['name'] }}</span>
                                <p class="task-text">{{ $task['text'] }}</p>
                                <div class="circle {{ $project['color'] }}"></div>
                                <span class="date">{{ $task['date'] }}</span>
                            </div>
                            @endforeach
                    @endforeach
                </div>
                <!-- End Archive -->
                <!-- Tasks be project -->
                @foreach($projects as $project)
                    <div class="tasks-container tab-pane" role="tabpanel" id="{{ $project['id'] }}_tab">
                        <!-- Expired tasks -->
                        @foreach($project['expired_tasks'] as $task)
                            <div class="box-footer box-comments expired">
                                <div class="square-{{ $task['priority'] }}"></div>
                                <span class="task-text">{{ $project['name'] }}</span>
                                <p class="task-text">{{ $task['text'] }}</p>
                                <div class="circle {{ $project['color'] }}"></div>
                                <span class="date">{{ $task['date'] }}</span>
                                <span class="glyphicon glyphicon-option-vertical"></span>
                                <ul class="menu">
                                    <button name="edit_task" value="{{ $task['id'] }}" class="col-md-12 btn btn-primary" type="button">Edit</button>
                                    <button name="done_task" value="{{ $task['id'] }}" class="col-md-12 btn btn-success" type="button">Done</button>
                                    <button name="delete_task" value="{{ $task['id'] }}" class="col-md-12 btn btn-danger" type="button">Delete</button>
                                </ul>
                            </div>
                        @endforeach
                        <!-- End expired tasks -->
                        @foreach($project['tasks'] as $task)
                            <div class="box-footer box-comments">
                                <div class="square-{{ $task['priority'] }}"></div>
                                <span class="task-text">{{ $project['name'] }}</span>
                                <p class="task-text">{{ $task['text'] }}</p>
                                <div class="circle {{ $project['color'] }}"></div>
                                <span class="date">{{ $task['date'] }}</span>
                                <span class="glyphicon glyphicon-option-vertical"></span>
                                <ul class="menu">
                                    <button name="edit_task" value="{{ $task['id'] }}" class="col-md-12 btn btn-primary" type="button">Edit</button>
                                    <button name="done_task" value="{{ $task['id'] }}" class="col-md-12 btn btn-success" type="button">Done</button>
                                    <button name="delete_task" value="{{ $task['id'] }}" class="col-md-12 btn btn-danger" type="button">Delete</button>
                                </ul>
                            </div>
                        @endforeach
                        <br>
                        <p class="{{ $project['id'] }} add-t">+Add task</p>
                    </div>
                @endforeach
                <!-- End tasks be project -->
                <!-- Delete task form -->
                <form id="delete_task" action="{{ route('taskDelete') }}" method="post" hidden>
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="">
                </form>
                <!-- Create task form -->
                <form id="create_task" action="{{ route('taskCreate') }}" method="post" hidden>
                    {{ csrf_field() }}
                    <input type="hidden" name="project_id">
                    <input class="col-md-12 form-control" type="text" name="text" placeholder="New task">
                    <label>Date</label>
                    <input class="form-control" type="date" name="date" value="">
                    <label>Priority</label>
                    <select class="form-control" name="priority">
                        <option value="3">Unimportant</option>
                        <option value="2">Important</option>
                        <option value="1">Very inportant</option>
                    </select>
                    <button class="col-md-6 btn btn-primary" type="submit" name="button">Create</button>
                    <button class="col-md-6 btn btn-danger" type="button" name="close_create_task">Close</button>
                </form>
                <!-- Update task form -->
                <form id="update_task" action="{{ route('taskUpdate') }}" method="post" hidden>
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="">
                    <input class="col-md-12 form-control" type="text" name="text" placeholder="Update task">
                    <label>Date</label>
                    <input class="form-control" type="date" name="date" value="">
                    <label>Priority</label>
                    <select class="form-control" name="priority">
                        <option value="3">Unimportant</option>
                        <option value="2">Important</option>
                        <option value="1">Very inportant</option>
                    </select>
                    <button class="col-md-6 btn btn-primary" type="submit" name="button">Edit</button>
                    <button class="col-md-6 btn btn-danger" type="button" name="close_update_task">Close</button>
                </form>
                <!-- Done task form -->
                <form id="done_task" action="{{ route('taskDone') }}" method="post" hidden>
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="">
                </form>
                <!-- Create task form for project -->
                <form id="create_task_for_project" action="{{ route('taskCreateForProject') }}" method="post" hidden>
                    {{ csrf_field() }}
                    <input class="col-md-12 form-control" type="text" name="text" placeholder="New task">
                    <label>Date</label>
                    <input class="form-control" type="date" name="date" value="">
                    <label>Priority</label>
                    <select class="form-control" name="priority">
                        <option value="3">Unimportant</option>
                        <option value="2">Important</option>
                        <option value="1">Very inportant</option>
                    </select>
                    <label>Project</label>
                    <select class="form-control" name="project_id">
                        @foreach($allProjects as $key => $value)
                            <option value="{{ $key }}">{{ $value }}</option>
                        @endforeach
                    </select>
                    <button class="col-md-6 btn btn-primary" type="submit" name="button">Create</button>
                    <button class="col-md-6 btn btn-danger" type="button" name="close_create_task_for_project">Close</button>
                </form>
            </div>
            <!-- End all tasks -->
        </div>
        <!-- End div .wrapper -->
        <!-- jQuery 3 -->
        <script src="{{ URL::asset('adminlte/bower_components/jquery/dist/jquery.min.js') }}"></script>
        <!-- Bootstrap 3.3.7 -->
        <script src="{{ URL::asset('adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
        <script src="{{ URL::asset('validation/dist/jquery.validate.js') }}"></script>
        <script src="{{ URL::asset('js/projectFunctions.js') }}"></script>
        <script src="{{ URL::asset('js/taskFunctions.js') }}"></script>
        <script src="{{ URL::asset('js/todoListAnimation.js') }}"></script>
    </body>
</html>
