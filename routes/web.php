<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Authentication
Route::get('/authentication', 'Auth\LoginController@showLoginForm')->name('authentication');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

// Login
Route::post('/login', 'Auth\LoginController@login')->name('login');
Route::get('/login', function(){
    return redirect()->route('authentication');
});

// Registration
Route::post('/register', 'Auth\RegisterController@register')->name('register');
Route::get('/register', function(){
    return redirect()->route('authentication');
});

// GOOGLE AUTHENTICATION
Route::get('login/google', 'Auth\GoogleController@redirectToProvider')->name('googleLogin');
Route::get('login/google/callback', 'Auth\GoogleController@handleProviderCallback');

// Reset password
Route::get('/authentication/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('/authentication/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('/authentication/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('/authentication/password/reset', 'Auth\ResetPasswordController@reset');

// Main page
Route::get('/', 'MainController@index')->name('todoList')->middleware('auth');

// Project actions
Route::post('/project/create', 'ProjectController@create')->name('projectCreate');
Route::get('/project/create', function(){
    return redirect()->route('todoList');
});
Route::post('/project/delete', 'ProjectController@delete')->name('projectDelete');
Route::get('/project/delete', function(){
    return redirect()->route('todoList');
});
Route::post('/project/update', 'ProjectController@update')->name('projectUpdate');
Route::get('/project/update', function(){
    return redirect()->route('todoList');
});

// Task actions
Route::post('/task/create', 'TaskController@create')->name('taskCreate');
Route::get('/task/create', function(){
    return redirect()->route('todoList');
});
Route::post('/task/delete', 'TaskController@delete')->name('taskDelete');
Route::get('/task/delete', function(){
    return redirect()->route('todoList');
});
Route::post('/task/create_for_project', 'TaskController@createForProject')->name('taskCreateForProject');
Route::get('/task/create_for_project', function(){
    return redirect()->route('todoList');
});
Route::post('/task/update', 'TaskController@update')->name('taskUpdate');
Route::get('/task/update', function(){
    return redirect()->route('todoList');
});
Route::post('/task/done', 'TaskController@done')->name('taskDone');
Route::get('/task/done', function(){
    return redirect()->route('todoList');
});
